import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';


function AboutUs (){

    return (
        <Box sx={{ width:"60%" }}>
             <Paper elevation={3}>
             <h2>คณะผู้จัดทำ เว็บนี้</h2>
            <h2>จัดทำโดย : นางสาวผกามาศ เดชสุวรรณ </h2>
            <h3>รหัส : 6310210267</h3>
            <h3>ติดต่อได้ที่ : 6310210267@psu.ac.th </h3>
            <h3>ที่อยู่ : ต.คอหงส์ อ.หาดใหญ่ จ.สงขลา 90110</h3>
            </Paper>
       </Box>    
    );
}

export default AboutUs;
