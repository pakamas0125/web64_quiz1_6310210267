import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import AboutUsPage from './page/AboutUsPage';
import NumberPage from './page/NumberPage';
import {Routes,Route} from "react-router-dom";

function App() {
  return (
    <div className="App">
    <Header/>
          <Routes>

         <Route path="about" element={
              <AboutUsPage/>
              }/>
         <Route path="/" element={
              <NumberPage/>
        }/>

      </Routes>
    </div>
  );
}

export default App;
